#include "Point2D.h"
#include "Vector2D.h"
#include "BoundingBox2D.h"
#include <cmath>
#include "Drawer2D.h"
Point2D::Point2D()
{
	this->m_x = nan("");
	this->m_y = nan("");

}

Point2D::Point2D(double x, double y):m_x(x), m_y(y)
{
}




Point2D::~Point2D()
{

}

bool Point2D::isValid() const
{
	if (isnan(m_x) && isnan(m_y))
	{
		return false;
	}
	return true;
}



double Point2D::getX() const
{
	return m_x;
}

double Point2D::getY() const
{
	return m_y;
}

Point2D Point2D::add(const Vector2D & vect) const
{

	if (this->isValid())
	{
		return Point2D(m_x+vect.getX(), m_y+vect.getY());
	}
	return Point2D();
}

Vector2D Point2D::getDifference(const Point2D & other) const
{
	if (other.isValid() && this->isValid())
	{
		return Vector2D(m_x-other.getX(), m_y-other.getY());
	}
	return Vector2D(nan(""), nan(""));
}

double Point2D::getDistance() const
{
	if (this->isValid())
	{
		return sqrt(m_x*m_x + m_y*m_y);
	}
	return nan("");
}

double Point2D::getDistance(const Point2D & other) const
{
	if (other.isValid() && this->isValid())
	{
		Vector2D v=this->getDifference(other);
		return v.getLength();
	}
	return nan("");
}

void Point2D::moveBy(const Vector2D & delta)
{
	if (this->isValid())
	{
		m_x += delta.getX();
		m_y += delta.getY();
	}
	
	
}

void Point2D::moveBy(double deltaX, double deltaY)
{
	if (this->isValid())
	{
		m_x += deltaX;
		m_y += deltaY;
	}
}

void Point2D::moveTo(double newX, double newY)
{
	m_x = newX;
	m_y = newY;
}

BoundingBox2D Point2D::getBounds() const
{
	

	return BoundingBox2D(m_x, m_y, m_x, m_y);
}

void Point2D::draw(Drawer2D & drawer) const
{

	drawer.drawPoint(m_x, m_y);

}
