#pragma once
//do we need an empty constr?
class Vector2D
{
public:
	Vector2D();
	Vector2D(double x=0.0, double y= 0.0);
	~Vector2D();
	double getX() const;
	double getY() const;
	Vector2D add(const Vector2D& other) const;
	Vector2D subtract(const Vector2D& other) const;
	double getLength() const;
	double getOrientation() const;
	double multiply(const Vector2D& other) const;
private:
	double m_x;
	double m_y;
};

