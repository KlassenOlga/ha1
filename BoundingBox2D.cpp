#include "BoundingBox2D.h"
#include <minmax.h>


BoundingBox2D::BoundingBox2D()
{
}

BoundingBox2D::BoundingBox2D(double maxX, double maxY, double minX, double minY):m_maxX(maxX), m_maxY(maxY), m_minX(minX), m_minY(minY)
{
	
}


BoundingBox2D::~BoundingBox2D()
{
}

double BoundingBox2D::getWidth() const
{
	
	return this->m_maxX-this->m_minX;
}

double BoundingBox2D::getHeight() const
{
	return this->m_maxY-this->m_minY;
}

void BoundingBox2D::uniteWith(const BoundingBox2D & other)
{
	m_maxX = max(this->m_maxX,other.m_maxX);
	m_maxY = max(this->m_maxY, other.m_maxY);
	m_minX = min(this->m_minX, other.m_minX);
	m_minY = min(this->m_minY, other.m_minY);
}

void BoundingBox2D::draw(Drawer2D & drawer) const
{
	
	drawer.drawBoundingBox(m_maxX, m_maxY, m_minX, m_minY);
}
