#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "Drawer2D.h"
#include "Turtleizer.h"
class DrawIt :
	public Drawer2D
{
public:
	void drawPoint(double x, double y)const override;
	void drawLine(double startX, double starY, double endX, double endY) const override;
	void drawBoundingBox(double maxX, double maxY, double minX, double minY) const override;
	//drawPolyline ist mit Hilfe der drawLine implementiert
	DrawIt();
	~DrawIt();
};

