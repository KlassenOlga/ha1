#include "Vector2D.h"
#define _USE_MATH_DEFINES
#include <cmath>

Vector2D::Vector2D(double x, double y):m_x(x), m_y(y)
{
}

Vector2D::Vector2D()
{
}


Vector2D::~Vector2D()
{
}

double Vector2D::getX() const
{
	return m_x;
}

double Vector2D::getY() const
{
	return m_y;
}

Vector2D Vector2D::add(const Vector2D & other) const
{
	double resX = m_x + other.m_x;
	double resY = m_y + other.m_y;
	Vector2D res(resX, resY);


	return res;
}

Vector2D Vector2D::subtract(const Vector2D & other) const
{
	double resX = m_x - other.m_x;
	double resY = m_y - other.m_y;
	return Vector2D(resX, resY);
}

double Vector2D::getLength() const
{

	return sqrt(m_x*m_x + m_y*m_y);
}

double Vector2D::getOrientation() const
{
	//erste variante
	double res = atan(m_y/m_x);
	if (m_x<0)
	{
		if (m_y<0)
		{
			res += M_PI;
		}
		else{
			res = M_PI - res;
		}
	}
	else{
		if (m_y<0)
		{
			res = 2 * M_PI - res;
		}
	}
	return res;
	//zweite m�gliche Variante
	//return atan2(m_x, m_y);
}

double Vector2D::multiply(const Vector2D & other) const
{
	return m_x*other.m_x+ m_y*other.m_y;
}
