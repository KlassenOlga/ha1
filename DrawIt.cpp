// #include "DrawIt.h"
// 
// #include "BoundingBox2D.h"
// #include "Point2D.h"
// #include "Line2D.h"
// #include "Vector2D.h"
// void DrawIt::drawPoint(double x, double y) const
// {
// 	
// 	double grad=90;
// 	float l=0.001;
// 	gotoXY(x, y);
// 	penDown();
// 	right(grad);
// 	forward(l/2);
// 	left(grad);
// 	forward(l/2);
// 	left(grad);
// 	forward(l);
// 	left(grad);
// 	forward(l);
// 	left(grad);
// 	forward(l);
// 	left(grad);
// 	forward(l / 2);
// 	left(grad);
// 	forward(l / 2);
// 	penUp();
// }
// 
// void DrawIt::drawLine(double startX, double starY, double endX, double endY) const
// {
// 	Point2D start(startX, starY);
// 	Point2D end(endX, endY);
// 	Line2D line(start, end);
// 	double angle = line.getVector().getOrientation();
// 	double length = line.getLength();
// 
// 	gotoXY(startX, starY);
// 	penDown();
// 	//davon ausgegangen, dass Turtle immer 90 Grad Startwinkel hat, ziehen wir 90 grad am start ab
// 	left(angle - 90);
// 	forward(length);
// 	penUp();
// 	
// 
// }
// 
// void DrawIt::drawBoundingBox(double maxX, double maxY, double minX, double minY) const
// {
// 	BoundingBox2D b(maxX, maxY, minX, minY);
// 
// 
// 	double width = b.getWidth();
// 	double height = b.getHeight();
// 	double grad = 90;
// 	gotoXY(minX, minY);
// 	penDown();
// 	right(grad);
// 	forward(width);
// 	left(grad);
// 	forward(height);
// 	left(grad);
// 	forward(width);
// 	left(grad);
// 	forward(height);
// 	penUp();
// }
// 
// DrawIt::DrawIt()
// {
// }
// 
// 
// DrawIt::~DrawIt()
// {
// }
