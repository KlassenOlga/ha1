#pragma once
class Vector2D;
class BoundingBox2D;
#include "Drawer2D.h"
class Point2D
{
public:
	Point2D();
	Point2D(double x, double y);

	~Point2D();
	bool isValid()const;

	double getX() const;
	double getY() const;
	
	Point2D add(const Vector2D& vect) const;
	Vector2D getDifference(const Point2D& other) const;
	double getDistance() const;
	double getDistance(const Point2D& other) const;
	void moveBy(const Vector2D& delta);
	void moveBy(double deltaX, double deltaY);
	void moveTo(double newX, double newY);
	BoundingBox2D getBounds() const;


	void draw(Drawer2D& drawer) const;

private:
	double m_x;
	double m_y;
};

