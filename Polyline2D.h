#pragma once
/*#include "Point2D.h"*/

#include "Line2D.h"
#include <vector>

using std::vector;
class Polyline2D:public Line2D
{
public:
	Polyline2D();
	Polyline2D(vector<Point2D> points, Point2D& begin, Point2D& end);
	~Polyline2D();
	/*Polyline2D add(vector<Point2D> points) const;*/
	Point2D getPoint(unsigned short number) const;//gibt pointer unter bestimmten Index zur�ck
	Point2D getPointAt(double offset) const override;//overriding of the funktion from the class Line2D
	unsigned short getNumberOfPoints() const;
	bool insertPoint(const Point2D& pt, unsigned short before);
	bool removePoint(unsigned short number);
	bool isSelfIntersecting() const;
	double getLength() const override;
	BoundingBox2D getBounds() const override ;
	void moveBy(const Vector2D& offs) override;
	void moveBy(double deltaX, double deltaY) override;
	double getDistance(const Point2D& other) const override;
	double getDistance(const Line2D& other) const override;

	void draw(Drawer2D& drawer) const;
private:
	vector<Point2D> m_points;
};

