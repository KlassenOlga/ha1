﻿
#include "Polyline2D.h"
#include "Point2D.h"
#include "Vector2D.h"
#include "BoundingBox2D.h"
#include <cmath>
#include <minmax.h>
Polyline2D::Polyline2D()
{ 
}

Polyline2D::Polyline2D(vector<Point2D> points, Point2D& begin, Point2D& end)
{
	if (begin.isValid())
	{
		m_pB.moveTo(begin.getX(), begin.getY());
	}
	if (end.isValid())
	{
		m_pE.moveTo(end.getX(), end.getY());
	}
	
	for (int i = 0;i<points.size(); i++)
	{
		
		if ( points[i].isValid())
		{
			m_points.push_back(points[i]);
			
		}
		
	}
}


Polyline2D::~Polyline2D()
{
}



Point2D Polyline2D::getPoint(unsigned short number) const
{
	if (number >= m_points.size())
	{
		return Point2D();
	}
	//Wurde nicht gesagt, dass der Point gelöscht werden muss
	return Point2D(m_points[number]);
}

Point2D Polyline2D::getPointAt(double offset) const 
{
	
	double length = 0.0;
	Vector2D vec= m_points[0].getDifference(m_pB);//ein Vector vom anfang bis zur ersten Linie
	length += vec.getLength();//davon Länge nehmen
	if (length>=offset)//wenn die Länge grösser ist, als unser offset, dann den neuen Punkt kriegen
	{
		double resVecX = offset / length * vec.getX();
		double resVecY = offset / length * vec.getY();
		Vector2D newVec(resVecX, resVecY);
		return(m_pB.add(newVec));
	}
	for (int i=1; i<getNumberOfPoints(); i++)//wenn nicht, gehen weiter
	{
		vec = m_points[i].getDifference(m_points[i-1]);
		length += vec.getLength();
		if (offset<=length)
		{
			double resVecX = offset / length * vec.getX();
			double resVecY = offset / length * vec.getY();
			Vector2D newVec(resVecX, resVecY);
			return(m_points[i-1].add(newVec));

		}
	}
	//else wenn es auch nicht der Fall war, dann vergleichen wir letzten punkt mit letzten aus dem array
	//last punkt

 	vec = m_pE.getDifference(m_points[m_points.size()-1]);
 	length += vec.getLength();
 	if (offset <= length)
 	{
		double resVecX = offset / length * vec.getX();
 		double resVecY = offset / length * vec.getY();
 		Vector2D newVec(resVecX, resVecY);
 		return(m_pB.add(newVec));
	}
	return Point2D();
}

unsigned short Polyline2D::getNumberOfPoints() const
{
	
	return m_points.size();
}

bool Polyline2D::insertPoint(const Point2D & pt, unsigned short before)
{
	if (pt.isValid())
	{
		m_points.insert(m_points.begin()+before, pt);
		return true;
	}


	return false;
}

bool Polyline2D::removePoint(unsigned short number)
{
	if (number>=m_points.size())
	{
		return false;
	}
	m_points.erase(m_points.begin() + number);
	return true;
}

bool Polyline2D::isSelfIntersecting() const
{


	double check=false;
	vector<Point2D> newArrayOfPoints(2);

	newArrayOfPoints.push_back(m_pB);
	
	for (int i = 0; i < m_points.size(); i++) {
		newArrayOfPoints.push_back(m_points[i]);
	}
	newArrayOfPoints.push_back(m_pE);


	for (int i=0; i<newArrayOfPoints.size()-1; i++)
	{
		for (int j = i+2; j < newArrayOfPoints.size()-1; j++) {
		
			Line2D line1(newArrayOfPoints[i], newArrayOfPoints[i + 1]);
			Line2D line2(newArrayOfPoints[j], newArrayOfPoints[j + 1]);
			check=line1.getDistance(line2);

			if (check==0)
			{
				return true;
			}
		}
	}


	return false;
}

double Polyline2D::getLength() const
{
	double length = 0.0;

		length = m_pB.getDistance(m_points[0]);
	
	for (int i = 0; i < m_points.size()-1; i++) {
		
			length += m_points[i].getDistance(m_points[i + 1]);
		
	}

		length += m_points[m_points.size() - 1].getDistance(m_pE);
	
	return length;
}

BoundingBox2D Polyline2D::getBounds() const
{

	//nur wenn begin und ende der Polyline exestieren, können wir mit ihr etwas tun
	if (m_pB.isValid()&& m_pE.isValid())
	{
		double maxX = max(m_pB.getX(), m_pE.getX());
		double minX = min(m_pB.getX(), m_pE.getX());
		double maxY = max(m_pB.getY(), m_pE.getY());
		double minY = min(m_pB.getY(), m_pE.getY());

		for (int i = 0; i < m_points.size(); i++) {
			maxX = max(maxX, m_points[i].getX());
			minX = min(minX, m_points[i].getX());
			maxY = max(maxY, m_points[i].getY());
			minY = min(minY, m_points[i].getY());
		}


		return BoundingBox2D(maxX, maxY, minX, minY);
	}

	return BoundingBox2D();
}

void Polyline2D::moveBy(const Vector2D & offs)
{
	m_pB.moveBy(offs);
	m_pE.moveBy(offs);

	for (int i=0; i<m_points.size(); i++)
	{
		
		m_points[i].moveBy(offs);
		
	}

}

void Polyline2D::moveBy(double deltaX, double deltaY)
{
	m_pB.moveTo(deltaX, deltaY);
	m_pE.moveTo(deltaX, deltaY);

	for (int i=0; i<m_points.size(); i++)
	{
		m_points[i].moveTo(deltaX, deltaY);
	}


}

double Polyline2D::getDistance(const Point2D & other) const
{
	double distance = 0.0;
	Point2D startPoint(m_pB);

	bool between_Start_End = false;

	Vector2D X0(m_pB.getX(), m_pB.getY());
	Vector2D a = m_points[0].getDifference(m_pB);
	Vector2D X1(other.getX(), other.getY());
	double t = a.multiply(X1.subtract(X0)) / a.multiply(a);

	Vector2D ta(a.getX()*t, a.getY()*t);
	Vector2D d = ta.add(X0.subtract(X1)); //Lotvector



	Point2D lotpunktAufGerade = other;
	lotpunktAufGerade.moveBy(d); //Lotpunkt

	Point2D otherPoint(other);
	Line2D lotlinie(otherPoint, lotpunktAufGerade); //prüft ob sich die lotlinie (von other zu Lotpunkt auf geraden) außerhalb der this->Linie befindet
	Point2D point0(m_points[0]);
	Line2D actual(startPoint, point0);//
	between_Start_End = actual.isPointBetweenStart_End(lotpunktAufGerade);

	if (between_Start_End == false) {
		if (m_pB.getDistance(other) > m_points[0].getDistance(other)) {
			distance = m_points[0].getDistance(other);
		}
		else {
			distance = m_pB.getDistance(other);
		}
	}
	else {

		distance = d.getLength();
	}

	for (int i = 0; i < m_points.size()-1;i++) {

		X0=m_points[i].getX(), m_points[i].getY();
		a = m_points[i+1].getDifference(m_points[i]);
		X1=other.getX(), other.getY();
		t = a.multiply(X1.subtract(X0)) / a.multiply(a);

		ta=a.getX()*t, a.getY()*t;
		d = ta.add(X0.subtract(X1)); //Lotvector



		lotpunktAufGerade = other;
		lotpunktAufGerade.moveBy(d); //Lotpunkt

		otherPoint = other;
		Line2D lotlinie(otherPoint, lotpunktAufGerade); //prüft ob sich die lotlinie (von other zu Lotpunkt auf geraden) außerhalb der this->Linie befindet
		Point2D pointi = m_points[i];
		Point2D pointi_1 = m_points[i+1];
		Line2D actual(pointi, pointi_1);//
		between_Start_End = actual.isPointBetweenStart_End(lotpunktAufGerade);

		if (between_Start_End == false) {
			if (pointi.getDistance(other) > pointi_1.getDistance(other)) {
				distance =min(distance, pointi_1.getDistance(other));
			}
			else {
				distance = min(distance,pointi.getDistance(other));
			}
		}
		else {

			distance = min(distance,d.getLength());
		}

	}

	//letzte punt aus array und endpunkt


	Vector2D X00(m_points[m_points.size()-1].getX(), m_points[m_points.size() - 1].getY());
	Vector2D aa = m_pE.getDifference(m_points[m_points.size() - 1]);
	Vector2D X11(other.getX(), other.getY());
	t = aa.multiply(X11.subtract(X00)) / aa.multiply(aa);

	Vector2D taa(aa.getX()*t, aa.getY()*t);
	Vector2D dd = taa.add(X00.subtract(X11)); //Lotvector



	Point2D lotpunktAufGeradeO = other;
	lotpunktAufGeradeO.moveBy(dd); //Lotpunkt

	Point2D otherPointOther(other);
	Line2D lotlinieOther(otherPointOther, lotpunktAufGeradeO); //prüft ob sich die lotlinie (von other zu Lotpunkt auf geraden) außerhalb der this->Linie befindet
	Point2D pointLast = m_points[m_points.size() - 1];
	Point2D point_End = m_pE;

	Line2D actualOther(pointLast, point_End);//
	between_Start_End = actualOther.isPointBetweenStart_End(lotpunktAufGeradeO);

	if (between_Start_End == false) {
		if (pointLast.getDistance(other) > point_End.getDistance(other)) {
			distance = min(distance, point_End.getDistance(other));
		}
		else {
			distance = min(distance, pointLast.getDistance(other));
		}
	}
	else {

		distance = min(distance, dd.getLength());
	}


	return distance;

}

double Polyline2D::getDistance(const Line2D & other) const
{
	Point2D start(m_pB);
	Point2D pointN(m_points[0]);
	Line2D actualLine(m_pB, m_points[0]);

	Point2D schnitt = actualLine.schnittPunktVonGeraden(other);
	double distance = 0.0;

	//parallel nach x achse
	if (isnan(schnitt.getX()))
	{

		distance= abs(other.getEnd().getX() - actualLine.getEnd().getX());


	}
	else if (isinf(schnitt.getX()))// parallel nach y achse
	{
		distance= abs(other.getEnd().getY() - actualLine.getEnd().getY());

	}
	else {
		distance = actualLine.getDistance(other.getStart());
		distance = min(distance, actualLine.getDistance(other.getEnd()));
	}
	for (int i = 0; i < m_points.size()-1; i++) {
		start.moveTo(m_points[i].getX(), m_points[i].getY());
		pointN.moveTo(m_points[i + 1].getX(), m_points[i + 1].getY());
		Line2D actualLine1(start,pointN);
		actualLine1.moveBy(m_points[i + 1].getDifference(m_points[i]));
		schnitt = actualLine1.schnittPunktVonGeraden(other);

		if (isnan(schnitt.getX()))
		{

			distance = min(distance, abs(other.getEnd().getX() - actualLine1.getEnd().getX()));


		}
		else if (isinf(schnitt.getX()))// parallel nach y achse
		{
			distance = min(distance,abs(other.getEnd().getY() - actualLine1.getEnd().getY()));

		}
		else{
			//are intersected
			if (actualLine1.isPointBetweenStart_End(schnitt) && other.isPointBetweenStart_End(schnitt))
			{
				return 0;
			}
			distance = min(distance, getDistance(other.getStart()));
			distance = min(distance, actualLine1.getDistance(other.getEnd()));
		}


	}


	start.moveTo(m_points[m_points.size()-1].getX(), m_points[m_points.size() - 1].getY());
	pointN.moveTo(m_pE.getX(), m_pE.getY());
	actualLine.moveBy(m_pE.getDifference(m_points[m_points.size()-1]));
	schnitt = actualLine.schnittPunktVonGeraden(other);
	//letzten und endpunkt vergleichen
	if (isnan(schnitt.getX()))
	{

		distance = min(distance, abs(other.getEnd().getX() - actualLine.getEnd().getX()));


	}
	else if (isinf(schnitt.getX()))// parallel nach y achse
	{
		distance = min(distance, abs(other.getEnd().getY() - actualLine.getEnd().getY()));

	}
	else {
		//are intersected
		if (actualLine.isPointBetweenStart_End(schnitt) && other.isPointBetweenStart_End(schnitt))
		{
			return 0;
		}
		distance = min(distance, getDistance(other.getStart()));
		distance = min(distance, actualLine.getDistance(other.getEnd()));
	}



	return distance;
	
}

void Polyline2D::draw(Drawer2D & drawer) const
{

	Point2D start(m_pB);
	Point2D zero(m_points[0]);
	Line2D lineStartZero(start, zero);
	double angle = lineStartZero.getVector().getOrientation();
	double length = lineStartZero.getLength();

	drawer.drawLine(m_pB.getX(), m_pB.getY(), m_points[0].getX(), m_points[0].getY());

	for (int i = 0; i < m_points.size()-1; i++) {

		drawer.drawLine(m_points[i].getX(), m_points[i].getY(), m_points[i+1].getX(), m_points[i+1].getY());
	}

	drawer.drawLine(m_points[m_points.size()-1].getX(), m_points[m_points.size()-1].getY(), m_pE.getX(), m_pE.getY());

}




