#pragma once
#include "Point2D.h"
#include <vector>
using std::vector;
class BoundingBox2D
{
public:
	BoundingBox2D();
	BoundingBox2D(double maxX, double maxY, double minX, double minY );
	~BoundingBox2D();
	double getWidth() const;
	double getHeight() const;
	void uniteWith(const BoundingBox2D& other);


	void draw(Drawer2D& drawer) const;
private:
	double m_maxX;
	double m_maxY;
	double m_minX;
	double m_minY;
};

