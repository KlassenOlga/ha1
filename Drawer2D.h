#pragma once

class Drawer2D
{
public:
	Drawer2D();
	~Drawer2D();
	
	virtual void drawPoint(double x, double y) const = 0;
	virtual void drawLine(double startX,double starY,double endX, double endY) const = 0;
	virtual void drawBoundingBox(double maxX, double maxY, double minX, double minY) const = 0;
};

