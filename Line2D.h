#pragma once
  
#include "Point2D.h"
#include "Drawer2D.h"
class Line2D{
public:
	Line2D(const Point2D& p1, const Point2D& p2);
	Line2D();
	~Line2D();
	Point2D getStart() const;
	Point2D getEnd() const;
	virtual Point2D getPointAt(double offset) const;
	Point2D getPointAtToPerpendikular(double offset) const;
	virtual double getLength() const;
	virtual Line2D add(const Vector2D& vect) const;// (verschiebt Endpunkt)
	Vector2D getVector() const;// (von Start - zu Endpunkt)
	virtual void moveBy(const Vector2D& offs);
	virtual void moveBy(double deltaX, double deltaY);
	virtual double getDistance(const Line2D& other) const;
	virtual double getDistance(const Point2D& other) const;
	virtual BoundingBox2D getBounds() const;
	bool isPointBetweenStart_End( Point2D & other) const;
	Point2D schnittPunktVonGeraden( const Line2D& other)const;

	void draw(Drawer2D& drawer) const;
protected:
	Point2D m_pB;
	Point2D m_pE;

};