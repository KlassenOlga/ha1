#include "Line2D.h"
#include"Vector2D.h"
#include "BoundingBox2D.h"
#include "DrawIt.h"
#include <cmath>
#include <algorithm>
#include <minmax.h>
Line2D::Line2D(){

}

Line2D::Line2D(const Point2D& p1,const Point2D& p2){
	if (p1.isValid()&& p2.isValid())
	{
		this->m_pB = p1;
		this->m_pE = p2;
	}

}

Line2D::~Line2D(){

}

Point2D Line2D::getStart() const
{
	return m_pB;
}

Point2D Line2D::getEnd() const
{
	return m_pE;
}
Point2D Line2D::getPointAt(double offset) const
{
	Vector2D newV = m_pE.getDifference(m_pB);//Ein Vektor zwisch 2 Punkte Anfang und Ende Bauen
	double lengthV = newV.getLength(); //Davon L�nge kriegen
	
	double resVecX = offset / lengthV * newV.getX();//Erfahren in wieviel Mal ist offset kleiner als L�nge, damit multiplizieren beide Katete
	double resVecY = offset / lengthV * newV.getY();//x und y

	Vector2D resVec(resVecX, resVecY);//neuen Vektor bauen
	Point2D newP(m_pB.add(resVec));

	Vector2D vectroFromStartToNewPoint =newP.getDifference(m_pB);
	if (vectroFromStartToNewPoint.getLength()>lengthV)
	{
		return Point2D();
	}
	return newP;
	
}
//Meine Variante mit Perpendicular
Point2D Line2D::getPointAtToPerpendikular(double offset) const
{
	double mX = (m_pB.getX() + m_pE.getX()) / 2;//X for middle point
	double mY = (m_pB.getY() + m_pE.getY()) / 2;//Y for middle point

	Point2D middlePoint(mX, mY);
	//Vector2D vec = middlePoint.getDifference(m_pB);
	Vector2D vec(mX - m_pB.getX(), mY - m_pB.getY());//Vector from the beginning to middle point
	Vector2D normalVec(vec.getY(), -vec.getX());//Normal vector from previous vector
	double normalVecLen = normalVec.getLength();
	
	double resVecX = offset / normalVecLen * normalVec.getX();//in how much is offset longer than normal vector length multiply by x
	double resVecY = offset / normalVecLen  * normalVec.getY();//in how much is offset longer than normal vector length multiply by x

	Vector2D resVector(resVecX, resVecY);//vector with the length of offset

	
	middlePoint.add(resVector);//new point 
	return middlePoint;
}

double Line2D::getLength() const
{

	return m_pB.getDistance(m_pE);
}

Line2D Line2D::add(const Vector2D & vect) const
{
	Point2D newPointBeginn(m_pB.getX(), m_pB.getY());//beginnPunkt bleibt unver�ndert
	Point2D newPointEnd=m_pE.add(vect);//endPunkt ver�ndert sich

	return Line2D(newPointBeginn, newPointEnd);
}

Vector2D Line2D::getVector() const
{
	//Vector vom Startpunkt zu Endpunkt
	Vector2D resultV = m_pE.getDifference(m_pB);

	return resultV;
}

void Line2D::moveBy(const Vector2D & offs)
{
	m_pB.add(offs);
	m_pE.add(offs);
}

void Line2D::moveBy(double deltaX, double deltaY)
{
	m_pB.moveBy(deltaX, deltaY);
	m_pE.moveBy(deltaX, deltaY);
}

double Line2D::getDistance(const Line2D & other) const
{

	Point2D schnitt = this->schnittPunktVonGeraden(other);

	//parallel nach x achse
	if (isnan(schnitt.getX()))
	{

			return abs(other.getEnd().getX()-this->getEnd().getX());
		

	}
	if (isinf(schnitt.getX()))
	{
		return abs(other.getEnd().getY() - this->getEnd().getY());

	}
		//are intersected
	if (this->isPointBetweenStart_End(schnitt) && other.isPointBetweenStart_End(schnitt))
	{
		return 0;
	}

	double distance = this->getDistance(other.getStart());
	distance = min(distance, this->getDistance(other.getEnd()));

	return distance;
}


Point2D Line2D:: schnittPunktVonGeraden(const Line2D& other)const
{

	int flag;


	double m1 = 0.0;
	double m2 = 0.0;
	double b1 = 0.0;
	double b2 = 0.0;
	double x = 0.0;
	double y = 0.0;
	//schnittpunktberechnung mit gleichungssystem
	if (m_pE.getX() != m_pB.getX() && other.getEnd().getX() != other.getStart().getX())
	{
		m1 = (m_pE.getY() - m_pB.getY()) / (m_pE.getX() - m_pB.getX());
		b1 = m_pE.getY() - (m1*m_pE.getX());
		m2 = (other.getEnd().getY() - other.getStart().getY()) / (other.getEnd().getX() - other.getStart().getX());
		b2 = other.getEnd().getY() - (m2*other.getEnd().getX());

		x = (b2 - b1) / (m1 - m2);
		y = m1 * x + b1;
	}
	else if (m_pE.getX() == m_pB.getX() && other.getEnd().getX() != other.getStart().getX())//x=const, there is no y
	{
		m2 = (other.getEnd().getY() - other.getStart().getY()) / (other.getEnd().getX() - other.getStart().getX());
		b2 = other.getEnd().getY() - (m2*other.getEnd().getX());
		x = m_pE.getX();
		y = m2 * x + b2;

	}
	else if (m_pE.getX() != m_pB.getX() && other.getEnd().getX() == other.getStart().getX())//x=const, there is no y
	{
		m1 = (m_pE.getY() - m_pB.getY()) / (m_pE.getX() - m_pB.getX());
		b1 = m_pE.getY() - (m1*m_pE.getX());
		x = other.getStart().getX();
		y = m1 * x + b1;

	}
	else {
		x = nan("Parallel");
		y = nan("Parallel");
	}
	return Point2D(x,y); 
}

void Line2D::draw(Drawer2D & drawer) const
{


	drawer.drawLine(m_pB.getX(), m_pB.getY(), m_pB.getX(), m_pE.getY());

}


double Line2D::getDistance(const Point2D & other) const
{
	double distance=0.0;

	bool between_Start_End=false;

	Vector2D X0(m_pB.getX(),m_pB.getY());
	Vector2D a = this->getVector();
	Vector2D X1(other.getX(), other.getY());
	double t = a.multiply(X1.subtract(X0)) / a.multiply(a);
	
	Vector2D ta(a.getX()*t, a.getY()*t);
	Vector2D d = ta.add(X0.subtract(X1)); //Lotvector



	Point2D lotpunktAufGerade = other;
	lotpunktAufGerade.moveBy(d); //Lotpunkt


	Line2D lotlinie(other, lotpunktAufGerade); //pr�ft ob sich die lotlinie (von other zu Lotpunkt auf geraden) au�erhalb der this->Linie befindet
	between_Start_End = this->isPointBetweenStart_End(lotpunktAufGerade);

	if (between_Start_End == false) {
		if (this->getStart().getDistance(other) > this->getEnd().getDistance(other)) {
			distance = this->getEnd().getDistance(other);
		}
		else {
			distance = this->getStart().getDistance(other);
		}
	}
	else {

		distance = d.getLength();
	}

	return distance;
}
bool Line2D::isPointBetweenStart_End( Point2D & other) const
{
	bool isBetween = false;


	Line2D linie_SO(m_pB, other);
	Line2D linie_EO(m_pE, other);
	
	Vector2D SO = linie_SO.getVector(); //Vektor von start zu other
	Vector2D EO = linie_EO.getVector(); //Vektor von end zu other

	if ((SO.getLength() <= this->getLength()) && (EO.getLength() <= this->getLength()))//<=
	{
		isBetween = true;
	}

	return isBetween;
}
BoundingBox2D Line2D::getBounds() const
{
	double maxX = max(m_pB.getX(), m_pE.getX());
	double minX = min(m_pB.getX(), m_pE.getX());
	double maxY = max(m_pB.getY(), m_pE.getY());
	double minY = min(m_pB.getY(), m_pE.getY());
	return BoundingBox2D(maxX, maxY, minX, minY);
}




